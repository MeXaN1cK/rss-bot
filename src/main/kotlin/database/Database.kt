package database

import log.Log
import org.sqlite.JDBC
import java.sql.Connection
import java.sql.DriverManager

class Database(private val filename: String) {
    private var connection: Connection? = null
    var articlesTable: ArticlesTable? = null
    var categoriesTable: CategoriesTable? = null
    var articlesCategoriesTable: ArticlesCategoriesTable? = null

    fun connect() {
        try {
            DriverManager.registerDriver(JDBC())
            connection = DriverManager.getConnection("jdbc:sqlite:$filename")
            initTables(connection)
            Log.info("Succesfully connected to the database: $filename")
        } catch (e: Exception) {
            Log.error("Cannot establish connection to the database!")
            Log.debug(e)
        }
    }

    private fun initTables(connection: Connection?){
        if (connection != null){
            articlesTable = ArticlesTable(connection)
            articlesTable?.init()
            categoriesTable = CategoriesTable(connection)
            categoriesTable?.init()
            articlesCategoriesTable = ArticlesCategoriesTable(connection)
            articlesCategoriesTable?.init()
        }else {
            Log.error("WTF? Database connection appears to be broken...")
        }
    }

    fun close() {
        connection?.close()
    }
}