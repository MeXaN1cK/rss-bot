package database

import log.Log
import java.sql.Connection
import java.sql.ResultSet
import java.sql.Statement

class ArticlesTable(connection: Connection) : Table(connection, "articles") {
    fun init(){
        query("CREATE TABLE IF NOT EXISTS articles\n" +
                "    (\n" +
                "        id               INTEGER CONSTRAINT articles_pk PRIMARY KEY AUTOINCREMENT\n" +
                "      , title            TEXT CONSTRAINT articles_title_not_null NOT NULL\n" +
                "      , url              TEXT CONSTRAINT articles_url_not_null NOT NULL CONSTRAINT articles_url_unique UNIQUE\n" +
                "      , publication_time TEXT\n" +
                "    )\n" +
                ";")
    }

    fun addToTable(title: String, url: String, pubDate: String): Long {
        var keys: Long = 0
        val stmt = connection.prepareStatement("INSERT INTO articles (title, url, publication_time) VALUES (?, ?, ?) ON CONFLICT (url) DO UPDATE SET title = ?, publication_time = ?;",
            Statement.RETURN_GENERATED_KEYS)
        stmt.setString(1,title)
        stmt.setString(2,url)
        stmt.setString(3,pubDate)
        stmt.setString(4,title)
        stmt.setString(5,pubDate)
        stmt.executeUpdate()
        val result = stmt.generatedKeys
        if(result.next()){
            keys = result.getLong(1)
        } else {
            Log.error("Result is empty");
        }
        result.close()
        stmt.closeOnCompletion()
        return keys
    }

    fun getArticleById(id: Long): List<String> {
        var result: List<String> = mutableListOf()
        val stmt = connection.prepareStatement("SELECT publication_time, url, title FROM $table WHERE id = ?")
        stmt.setLong(1, id)
        val rs =  stmt.executeQuery()
        while(rs!!.next()){
            val el = "${rs.getString("publication_time")} ${rs.getString("url")} ${rs.getString("title")}"
            result = result.plus(el)
        }
        rs.close()
        stmt.closeOnCompletion()
        return result
    }

    fun getArticleByPubTime(pubDate: String): List<String> {
        var result: List<String> = mutableListOf()
        val stmt = connection.prepareStatement("SELECT publication_time, url, title FROM $table WHERE publication_time = ?")
        stmt.setString(1, pubDate)
        val rs =  stmt.executeQuery()
        while(rs!!.next()){
            val el = "${rs.getString("publication_time")} ${rs.getString("url")} ${rs.getString("title")}"
            result = result.plus(el)
        }
        rs.close()
        stmt.closeOnCompletion()
        return result
    }
}