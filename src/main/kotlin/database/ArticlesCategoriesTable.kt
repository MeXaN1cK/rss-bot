package database

import log.Log
import java.sql.Connection
import java.sql.ResultSet
import java.sql.Statement

class ArticlesCategoriesTable(connection: Connection): Table(connection,"article_categories") {
    fun init(){
        query("CREATE TABLE IF NOT EXISTS $table (\n" +
                "    id INTEGER\n" +
                "        CONSTRAINT article_categories_pk\n" +
                "            PRIMARY KEY AUTOINCREMENT,\n" +
                "    article_id INTEGER\n" +
                "        CONSTRAINT article_categories_article_id_not_null NOT NULL\n" +
                "        CONSTRAINT article_categories_article_id_fk\n" +
                "            REFERENCES articles\n" +
                "            ON DELETE CASCADE,\n" +
                "    category_id INTEGER\n" +
                "        CONSTRAINT article_categories_category_id_not_null NOT NULL\n" +
                "        CONSTRAINT article_categories_category_id_fk\n" +
                "            REFERENCES categories\n" +
                "            ON DELETE CASCADE,\n" +
                "    CONSTRAINT article_categories_article_id_category_id_unique\n" +
                "        UNIQUE (article_id, category_id)\n" +
                ");")
    }

    fun addToTable(article_id: Long, category_id: Long): List<Long>{
        val keys: MutableList<Long> = mutableListOf()
        val stmt = connection.prepareStatement("INSERT INTO $table (article_id, category_id) VALUES (?, ?) ON CONFLICT DO NOTHING",
            Statement.RETURN_GENERATED_KEYS)
        stmt.setLong(1,article_id)
        stmt.setLong(2,category_id)
        stmt.executeUpdate()
        val result = stmt.generatedKeys
        if(result.next()){
            keys.add(result.getLong(1))
        } else {
            Log.error("Result is empty");
        }
        result.close()
        stmt.closeOnCompletion()
        return keys
    }

    fun getArticleByWebsiteAndCategory(website: String, category: String): List<String> {
        var result: List<String> = mutableListOf()
        val stmt = connection.prepareStatement("select a.publication_time, a.url, a.title from articles a, categories c, article_categories ac where a.id = ac.article_id and c.id = ac.category_id and ac.category_id in\n" +
                "(select id from categories where website like ? and category like ? )")
        stmt.setString(1, website)
        stmt.setString(2, category)
        val rs = stmt.executeQuery()
        while(rs!!.next()){
            val el = "${rs.getString("publication_time")} ${rs.getString("url")} ${rs.getString("title")}"
            result = result.plus(el)
        }
        rs.close()
        stmt.closeOnCompletion()
        return result
    }

    fun getArticleByWebsite(website: String): List<String> {
        var result: List<String> = mutableListOf()
        val stmt = connection.prepareStatement("select a.publication_time, a.url, a.title from articles a, categories c, article_categories ac where a.id = ac.article_id and c.id = ac.category_id and ac.category_id in\n" +
                "(select id from categories where website like ? )")
        stmt.setString(1, website)
        val rs =  stmt.executeQuery()
        while(rs!!.next()){
            val el = "${rs.getString("publication_time")} ${rs.getString("url")} ${rs.getString("title")}"
            result = result.plus(el)
        }
        rs.close()
        stmt.closeOnCompletion()
        return result
    }

    fun getArticleByCategory(category: String): List<String> {
        var result: List<String> = mutableListOf()
        val stmt = connection.prepareStatement("select a.publication_time, a.url, a.title from articles a, categories c, article_categories ac where a.id = ac.article_id and c.id = ac.category_id and ac.category_id in\n" +
                "(select id from categories where category like ? )")
        stmt.setString(1, category)
        val rs =  stmt.executeQuery()
        while(rs!!.next()){
            val el = "${rs.getString("publication_time")} ${rs.getString("url")} ${rs.getString("title")}"
            result = result.plus(el)
        }
        rs.close()
        stmt.closeOnCompletion()
        return result
    }
}