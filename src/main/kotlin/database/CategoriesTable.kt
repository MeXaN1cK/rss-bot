package database

import log.Log
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement

class CategoriesTable(connection: Connection) : Table(connection, "categories") {
    fun init(){
        query("CREATE TABLE IF NOT EXISTS $table (\n" +
                "    id INTEGER\n" +
                "        CONSTRAINT categories_pk\n" +
                "            PRIMARY KEY AUTOINCREMENT,\n" +
                "    website TEXT\n" +
                "        CONSTRAINT categories_website_not_null NOT NULL,\n" +
                "    category TEXT\n" +
                "        CONSTRAINT categories_name_not_null NOT NULL,\n" +
                "    url TEXT\n" +
                "        CONSTRAINT categories_url_not_null NOT NULL,\n" +
                "    CONSTRAINT categories_website_category_unique\n" +
                "        UNIQUE (website, category)\n" +
                ");")
    }

    fun addToTable(website: String, category: String, url: String): List<Long>{
        val keys: MutableList<Long> = mutableListOf()
        val stmt = connection.prepareStatement("INSERT INTO $table (website, category, url) VALUES (?, ?, ?)",
            Statement.RETURN_GENERATED_KEYS)
        stmt.setString(1,website)
        stmt.setString(2,category)
        stmt.setString(3,url)
        stmt.executeUpdate()
        val result = stmt.generatedKeys
        if(result.next()){
            keys.add(result.getLong(1))
        } else {
            Log.error("Result is empty");
        }
        result.close()
        stmt.closeOnCompletion()
        return keys
    }

    fun getWebsiteByCategory(category: String): List<String> {
        var result: List<String> = mutableListOf()
        val stmt = connection.prepareStatement("SELECT website FROM $table WHERE category = ?")
        stmt.setString(1, category)
        val rs = stmt.executeQuery()
        while (rs!!.next()){
            result = result.plus(rs.getString("website"))
        }
        return result
    }

    fun getCategoryByWebsite(website: String): List<String> {
        var result: List<String> = mutableListOf()
        val stmt = connection.prepareStatement("SELECT category FROM $table WHERE website = ?")
        stmt.setString(1, website);
        val rs = stmt.executeQuery()
        while (rs!!.next()){
            result = result.plus(rs.getString("category"))
        }
        return result
    }

    fun getWebsites(): List<String>{
        var rs: ResultSet? = null
        var result: List<String> = mutableListOf()
        try {
            val statement = connection.createStatement()
            rs = statement.executeQuery("select distinct(website) from categories")
            statement.closeOnCompletion()
        } catch (e: SQLException) {
            e.printStackTrace()
        }
        while(rs!!.next()){
            result = result.plus(rs.getString("website"))
        }
        rs.close()
        return result
    }

    fun getCategories(): List<String> {
        var rs: ResultSet? = null
        var result: List<String> = mutableListOf()
        try {
            val statement = connection.createStatement()
            rs = statement.executeQuery("select distinct(category) from categories")
            statement.closeOnCompletion()
        } catch (e: SQLException) {
            e.printStackTrace()
        }
        while(rs!!.next()){
            result = result.plus(rs.getString("category"))
        }
        rs.close()
        return result
    }

    fun getIdByWebsiteAndCategory(website: String,category: String): Long{
        var result: Long = 0
        val stmt = connection.prepareStatement("SELECT id FROM $table WHERE website = ? AND category = ?")
        stmt.setString(1, website)
        stmt.setString(2, category)
        val rs = stmt.executeQuery()
        while (rs!!.next()){
            result = rs.getLong("id")
        }
        return result
    }

    fun getUrlByWebsite(website: String): List<String>{
        var result: List<String> = mutableListOf()
        val stmt = connection.prepareStatement("SELECT url FROM $table WHERE website = ?")
        stmt.setString(1, website);
        val rs = stmt.executeQuery()
        while (rs!!.next()){
            result = result.plus(rs.getString("url"))
        }
        rs.close()
        return result
    }
}