package database

import log.Log
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement

open class Table(val connection: Connection, val table: String) {
    private val timeout = 30

    fun query(sql: String): ResultSet {
        val statement = connection.createStatement()
        statement.queryTimeout = timeout
        statement.executeUpdate(sql)
        val result = statement.generatedKeys
        statement.closeOnCompletion()
        return result
    }

    fun select(sql: String): ResultSet? {
        var rs: ResultSet? = null
        try {
            val statement = connection.createStatement()
            statement.queryTimeout = timeout
            rs = statement.executeQuery(sql)
            statement.closeOnCompletion()
        } catch (e: SQLException) {
            e.printStackTrace()
        }
        return rs
    }

    fun insert(sql: String): List<Long> {
        val keys: MutableList<Long> = mutableListOf()
        try {
            val statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
            statement.execute()
            val result = statement.generatedKeys
            if(result.next()){
                keys.add(result.getLong(1))
            } else {
                Log.error("Result is empty");
            }
            result.close()
            statement.closeOnCompletion()
        } catch (e: SQLException) {
            e.printStackTrace()
        }
        return keys
    }
}
