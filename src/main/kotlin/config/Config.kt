package config

import java.io.FileInputStream
import java.util.*

object Config {
    private val prop = Properties()

    private const val IRC_NICK_KEY = "irc_nick"
    private const val IRC_NAME_KEY = "irc_name"
    private const val IRC_USER_KEY = "irc_user"
    private const val IRC_REALNAME_KEY = "irc_realname"
    private const val IRC_PASSWORD_KEY = "irc_password"
    private const val IRC_CHANNEL_KEY = "irc_channel"
    private const val IRC_HOST_KEY = "irc_host"
    private const val DATABASE_FILE = "database_file"

    var irc_nick: String? = null
    var irc_channel: String? = null
    var irc_host: String? = null
    var irc_name: String? = null
    var irc_user: String? = null
    var irc_realname: String? = null
    var irc_password: String? = null
    var database_file: String? = null

    fun load(filename: String) {
        FileInputStream(filename).use {
            prop.load(it)
            irc_nick = getString(IRC_NICK_KEY, "MeXRssBot")
            irc_name = getString(IRC_NAME_KEY, "MeXRssBot")
            irc_user = getString(IRC_USER_KEY, "MeXRssBot")
            irc_realname = getString(IRC_REALNAME_KEY, "MeX the Bot")
            irc_password = getString(IRC_PASSWORD_KEY, null)
            irc_channel = getString(IRC_CHANNEL_KEY, null)
            irc_host = getString(IRC_HOST_KEY, "eu.irc.esper.net")
            database_file = getString(DATABASE_FILE,null)
        }
    }

    private fun getString(key: String, default: String?): String? {
        return prop.getProperty(key) ?: default
    }
}