package log

enum class LogLevel {
    DEBUG, INFO, WARNING, ERROR
}