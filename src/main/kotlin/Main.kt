import client.IrcManager
import config.Config
import database.Database

class Main {
    init {
        Config.load("Config.properties")
        val ircManager = IrcManager(
            Config.irc_host!!,
            Config.irc_channel!!,
            Config.irc_password!!,
            Config.irc_nick!!,
            Config.irc_name!!,
            Config.irc_user!!,
            Config.irc_realname!!
        )
        val database = Config.database_file?.let { Database(it) }
        database?.connect()
        if (database != null) {
            ircManager.setDatabase(database)
        }
    }
}

fun main(args: Array<String>) {
    Main()
}

