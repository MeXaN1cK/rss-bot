package client

import com.apptastic.rssreader.Item
import com.apptastic.rssreader.RssReader
import com.github.kittinunf.fuel.core.InlineDataPart
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpUpload
import com.github.kittinunf.result.Result
import database.Database
import log.Log
import net.engio.mbassy.listener.Handler
import org.jsoup.Jsoup
import org.kitteh.irc.client.library.Client
import org.kitteh.irc.client.library.event.channel.ChannelMessageEvent
import org.kitteh.irc.client.library.event.connection.ClientConnectionClosedEvent
import org.kitteh.irc.client.library.event.connection.ClientConnectionEndedEvent
import org.kitteh.irc.client.library.event.connection.ClientConnectionFailedEvent
import org.kitteh.irc.client.library.feature.auth.NickServ
import java.nio.ByteBuffer
import java.nio.CharBuffer
import java.nio.charset.Charset
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.stream.Stream
import kotlin.math.abs
import kotlin.streams.toList


/* https://3dnews.ru/news/rss/ новости
*  https://3dnews.ru/hardware-news/rss железо
*  http://www.ixbt.com/export/news.rss новости
*  http://www.ixbt.com/export/hardnews.rss железо
*  https://www.cbr-xml-daily.ru/daily_eng_utf8.xml курсы валют
*  https://habr.com/ru/rss/best/daily/?fl=ru топовые новости с хабра.
*/

private const val ZERO_WIDTH_SPACE = 0x200B.toChar()
private val CHARSET = Charsets.UTF_8
private const val MESSAGE_MAX_BYTES_LENGTH = 400
const val CLBIN_URL = "https://clbin.com/"

class IrcManager(
    host: String,
    private val channel: String,
    password: String,
    nick: String,
    name: String,
    user: String,
    realname: String
) {
    private val ircColors: Array<String> = arrayOf(
        "\u0002",
        "\u001D",
        "\u001F",
        "\u000399",
        "\u000302",
        "\u000303",
        "\u000304",
        "\u000305",
        "\u000306",
        "\u000307",
        "\u000308",
        "\u000309",
        "\u000310",
        "\u000311",
        "\u000312",
        "\u000313"
    )
    private val client: Client = Client.builder()
        .nick(nick)
        .name(name)
        .user(user)
        .realName(realname)
        .server()
        .host(host)
        .then().buildAndConnect()

    private var database: Database? = null

    init {
        client.eventManager.registerEventListener(this)
        client.authManager.addProtocol(
            NickServ.builder(client).account(nick).password(password).build()
        ); // - auth on Nickserv
        client.addChannel(channel)
    }

    fun setDatabase(database: Database) {
        this.database = database
    }

    private fun hashOfNick(nickname: String): Int {
        val hash = nickname.hashCode()
        return (abs(hash) % ircColors.size) // Return index of color in ircColors from nickname
    }

    private fun sendMessage(message: String?) {
        if (message != null) {
            client.sendMessage(channel, message)
        }
    }

    private fun SplitStringByByteLength(src: String, encoding: String?, maxsize: Int): Array<String> {
        val cs = Charset.forName(encoding)
        val coder = cs.newEncoder()
        val out = ByteBuffer.allocate(maxsize) // output buffer of required size
        val `in` = CharBuffer.wrap(src)
        val ss: MutableList<String> = ArrayList() // a list to store the chunks
        var pos = 0
        while (true) {
            val cr = coder.encode(`in`, out, true) // try to encode as much as possible
            val newpos = src.length - `in`.length
            val s = src.substring(pos, newpos)
            ss.add(s) // add what has been encoded to the list
            pos = newpos // store new input position
            out.rewind() // and rewind output buffer
            if (!cr.isOverflow) {
                break // everything has been encoded
            }
        }
        return ss.toTypedArray()
    } // cut message by bytes

    @Handler
    fun onClientConnectionClose(event: ClientConnectionClosedEvent) {
        Log.error("Connection close event! : ${event.cause}")
        event.client.reconnect()
    }

    @Handler
    fun onClientConnectionEnded(event: ClientConnectionEndedEvent) {
        Log.error("Connection ended event! : ${event.cause}")
        event.client.reconnect()
    }

    @Handler
    fun onClientConnectionFailed(event: ClientConnectionFailedEvent) {
        Log.error("Connection failed event! : ${event.cause}")
        event.client.reconnect()
    }

    @Handler
    fun incoming(event: ChannelMessageEvent) {
        if (event.message.startsWith("#")) process(event.message.drop(1))
    }
    // 0    1       2   3
    // #rss 3dnews news 5

    private fun helpMessage() {
        val strToClbin = """
            Чтобы использовать Rss-бота используйте следующие команды: 
            #rss websites - Выведет названия сайтов, на которые подписан бот.
            #rss categories - Выведет названия категорий, которые предоставляют сайты.
            #rss Website - Выведет все категории, по сайту Website, на которые подписан бот.
                Например #rss 3dnews - выведет все категории, с сайта 3dnews.
            #rss Category - Выведет все сайты, по категории Category, на которые подписан бот.
                Например #rss hardware - Выведет все сайты, где есть категория hardware.
            #rss Website Category - Выведет первые 5 статей с сайта Website и по категории Category, остальное предоставит по отдельной ссылке.
                Например #rss 3dnews hardware - Выведет все статьи, с сайта 3dnews и по категории hardware.
            #rss Website Category Count - Выведет N статей с сайта Website и по категории Category, остальное предоставит по отдельной ссылке.
                Например #rss 3dnews hardware 5 - Выведет 5 статей, с сайта 3dnews и по категории hardware.
                Если хотите получить все новости с этого сайта, то вместо числа указывайте all.
            #rssAdd Website Category Url - Добавляет новый сайт с Rss рассылкой, по названию сайта Website, категории Category и ссылке Url.
                Например #rss ixbt hardware http://www.ixbt.com/export/hardnews.rss добавит сайт ixbt с категорией hardware.
            #rssUpdate Website Category - Загрузит новые статьи с сайта Website по категории Category
                Например #rssUpdate ixbt hardware - Обновит статьи с сайта ixbt по категории hardware
            #about - Предоставит информацию о боте, его авторе и исходному коду.
            #help - Выведет данной сообщение.
            #course - Курсы валют(пока не реализовано).
        """.trimIndent()
        sendMessage("Полный список команд доступен по ссылке: ${sendToClbin(strToClbin)}")
    }

    // 0-rss 1-site 2-category 3-count
    private fun process(message: String) {
        lateinit var sites: List<String>
        lateinit var categories: List<String>
        val words = message.split(' ', '\t', '\r', '\n').filterNot { it.isEmpty() }
        if (words.isNotEmpty()) {
            when (words.first()) {
                "rss" -> {
                    when (words.elementAt(1)) {
                        "websites" -> {
                            val sites = database?.categoriesTable?.getWebsites()
                            var res = ""
                            sites?.forEach { it -> res += "$it, " }
                            sendMessage("Сайты, на которые подписан бот: $res")
                        }
                        "categories" -> {
                            val categories = database?.categoriesTable?.getCategories()
                            var res = ""
                            categories?.forEach { it -> res += "$it, " }
                            sendMessage("Категории, на которые подписан бот: $res")
                        }
                        else -> {
                            sites = database?.categoriesTable?.getWebsites()!!
                            categories = database?.categoriesTable?.getCategories()!!
                            val resSites = sites.find { site -> words.elementAt(1) == site }
                            val resCategories = categories.find { category -> words.elementAt(1) == category }
                            if (resSites != null) {
                                val categorys = database?.categoriesTable?.getCategoryByWebsite(resSites)
                                var res = ""
                                categorys?.forEach { it -> res += "$it, " }
                                sendMessage("Категории, сайта $resSites: $res")
                            }
                            if (resCategories != null) {
                                val sits = database?.categoriesTable?.getWebsiteByCategory(resCategories)
                                var res = ""
                                sits?.forEach { it -> res += "$it, " }
                                sendMessage("Сайты, по категории $resCategories: $res")
                            } else {
                                if (words.elementAt(2) == categories.find { category -> words.elementAt(2) == category }) {
                                    val regex = "(https?:\\/\\/(?:[a-zA-Z]+\\.)?[a-zA-Z]+\\.\\S*)(\\/\\S*)".toRegex()
                                    val resCategory = categories.find { category -> words.elementAt(2) == category }
                                    lateinit var articles: List<String>
                                    var strToClbin = ""
                                    if (resCategory != null) {
                                        articles =
                                            database?.articlesCategoriesTable?.getArticleByWebsiteAndCategory(
                                                words.elementAt(1), resCategory
                                            )!!
                                        if (words.elementAt(3).lowercase(Locale.getDefault()) == "all"){
                                            articles.take(5).forEach {
                                                sendMessage(
                                                    regex.replace(
                                                        it,
                                                        regex.find(it)?.value?.let { it1 -> shortUrl(it1) }.toString()
                                                    )
                                                )
                                            }
                                            articles.forEach {
                                                strToClbin += "${
                                                    regex.replace(
                                                        it,
                                                        regex.find(it)?.value?.let { it1 -> shortUrl(it1) }.toString()
                                                    )
                                                } \n"
                                            }
                                            sendMessage("Полный вывод по ссылке: ${sendToClbin(strToClbin)}")
                                        } else if (words.elementAt(3).toInt() > 5) {
                                            articles.take(5).forEach {
                                                sendMessage(regex.replace(it,
                                                    regex.find(it)?.value?.let { it1 -> shortUrl(it1) }.toString()
                                                )
                                                )
                                            }
                                            articles.take(words.elementAt(3).toInt()).forEach {
                                                strToClbin += "${
                                                    regex.replace(
                                                        it,
                                                        regex.find(it)?.value?.let { it1 -> shortUrl(it1) }
                                                            .toString()
                                                    )
                                                } \n"
                                            }
                                            sendMessage("Полный вывод по ссылке: ${sendToClbin(strToClbin)}")
                                        } else if (words.elementAt(3).toInt() <= 5) {
                                            articles.take(words.elementAt(3).toInt()).forEach {
                                                sendMessage(
                                                    regex.replace(
                                                        it,
                                                        regex.find(it)?.value?.let { it1 -> shortUrl(it1) }
                                                            .toString()
                                                    )
                                                )
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                "rssAdd" -> addRss(words.elementAt(1), words.elementAt(2), words.elementAt(3))
                "rssUpdate" -> updateRss(words.elementAt(1), words.elementAt(2))
                "about" -> sendMessage("about")
                "help" -> helpMessage()
                "course" -> sendMessage("course")
                else -> helpMessage()
            }
        } else helpMessage()
    }

    private fun addRss(rssSiteName: String, rssCategory: String, url: String) {
        if (database?.categoriesTable?.getIdByWebsiteAndCategory(rssSiteName, rssCategory)?.compareTo(0) == 0) {
            database?.categoriesTable?.addToTable(rssSiteName, rssCategory, url)
            sendMessage("Сайт $rssSiteName с категорией $rssCategory успешно добавлен!")
        } else {
            sendMessage("Сайт $rssSiteName с категорией $rssCategory уже существует!")
        }
    }

    private fun updateRss(rssSiteName: String, rssCategory: String) {
        val reader = RssReader()
        lateinit var rssFeed: Stream<Item>

        val websitesUrls: List<String> = database?.categoriesTable?.getUrlByWebsite(rssSiteName)!!
        val websitesIds: Long? = database?.categoriesTable?.getIdByWebsiteAndCategory(rssSiteName, rssCategory)

        websitesUrls.forEach { site ->
            rssFeed = reader.read(site)
            val rssList = rssFeed.toList()
            rssList.forEach { rss ->
                val id = database?.articlesTable?.addToTable(
                    rss.title.get(),
                    rss.link.get(),
                    rss.pubDateZonedDateTime.get().format(DateTimeFormatter.ofPattern("d-M-Y HH:mm Z"))
                )
                if (id != null) {
                    if (websitesIds != null) {
                        database?.articlesCategoriesTable?.addToTable(id, websitesIds)
                    }
                }
            }
        }

        sendMessage("Обновлены статьи сайта $rssSiteName по категории $rssCategory!")
    }

    private fun shortUrl(url: String): String {
        var shortUrl = ""
        val httpAsync = "https://da.gd/s/?url=$url"
            .httpGet()
            .responseString { request, response, result ->
                when (result) {
                    is Result.Failure -> {
                        val ex = result.getException()
                        println(ex)
                    }
                    is Result.Success -> {
                        val data = result.get()
                        val doc = Jsoup.parse(data)
                        shortUrl = doc.selectXpath("//div[@id='app']//a").text()
                    }
                }
            }

        httpAsync.join()
        return shortUrl
    }

    private fun sendToClbin(data: String): String {
        var res = ""
        val httpAsync = CLBIN_URL
            .httpUpload()
            .add(InlineDataPart(data, name = "clbin"))
            .responseString { _, _, result ->
                when (result) {
                    is Result.Failure -> {
                        val ex = result.getException()
                        println(ex)
                    }
                    is Result.Success -> {
                        res = result.get().trim()
                    }
                }
            }
        httpAsync.join()
        return res
    }

}
