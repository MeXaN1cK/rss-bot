import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

apply(plugin = "idea")
apply(plugin = "java")
apply(plugin = "kotlin")
apply(plugin = "application")


plugins {
    kotlin("jvm") version "1.5.31"
    id("com.github.johnrengelman.shadow") version("7.1.0")
}

group = "me.vojak"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://jitpack.io")
    jcenter()
}

dependencies {
    testImplementation(kotlin("test"))
}

dependencies {
    implementation(group = "com.apptastic",name = "rssreader","2.5.0")
    implementation(group = "org.kitteh.irc", name = "client-lib","8.0.0")
    implementation(group = "org.xerial", name = "sqlite-jdbc","3.34.0")
    implementation(group = "com.github.kittinunf.fuel", name = "fuel", version = "2.3.1")
    implementation(group = "org.jsoup", name = "jsoup", version = "1.14.3")
}

tasks.test {
    useJUnit()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}